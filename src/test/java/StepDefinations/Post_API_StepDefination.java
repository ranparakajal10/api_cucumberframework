package StepDefinations;

import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;

import common_method_package.Trigger_Api_Method;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import request_repository.Endpoint;
import request_repository.Post_request_repository;

public class Post_API_StepDefination {
	String req_body;
	String Post_Endpoint;
	int res_StatusCode;
	String res_body;

	@Given("Enter the name and job in requestbody")
	public void enter_the_name_and_job_in_requestbody() throws IOException {
		req_body = Post_request_repository.post_TC1_Request();
		Post_Endpoint = Endpoint.post_endpoint();
		// throw new io.cucumber.java.PendingException();
	}

	@When("Send the post request with requestbody")
	public void send_the_post_request_with_requestbody() {
		res_StatusCode = Trigger_Api_Method.extract_Status_Code(req_body, Post_Endpoint);
		res_body = Trigger_Api_Method.extract_Responsebody(req_body, Post_Endpoint);
		// throw new io.cucumber.java.PendingException();
	}

	@Then("Success response is received {int} as status code")
	public void success_response_is_received_as_status_code(Integer int1) {
		Assert.assertEquals(res_StatusCode, 201);
		// throw new io.cucumber.java.PendingException();
	}

	@Then("Response body as per API guide document")
	public void response_body_as_per_api_guide_document() {
		JsonPath jsp_req = new JsonPath(req_body);

		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		JsonPath jsp_res = new JsonPath(res_body);

		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");
		String res_id = jsp_res.getString("id");

		LocalDateTime CurrentDate = LocalDateTime.now();
		String ExpectedDate = CurrentDate.toString().substring(0, 11);
		String res_createdAt = jsp_res.getString("createdAt").substring(0, 11);

		// Validation
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertNotNull(res_id);
		Assert.assertEquals(ExpectedDate, res_createdAt);

		// throw new io.cucumber.java.PendingException();
	}
}
