package common_method_package;

import static io.restassured.RestAssured.given;

import request_repository.Endpoint;

public class Trigger_Api_GetMethod extends Endpoint
{
	public static int extract_Status_Code(String URL) {
		int StatusCode = given().header("Content-Type", "application/json").when().get(URL).then()
				.extract().statusCode();
		return StatusCode;

	}

	public static String extract_Responsebody(String URL) {

		String Responsebody = given().header("Content-Type", "application/json").log().all().when()
				.get(URL).then().log().all().extract().response().asString();
		return Responsebody;

	}
}
