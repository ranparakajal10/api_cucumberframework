package common_utility_package;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Excel_Data_Reader {
	public static ArrayList<String> Read_Excel_Data(String File_name, String Sheet_name, String Test_Case_Name)
			throws IOException {

		ArrayList<String> ArrayData = new ArrayList<String>();
		// Step1:Locate the file
		String Project_dir = System.getProperty("user.dir");
		FileInputStream fis = new FileInputStream(Project_dir + "\\Input_data\\" + File_name);

		// Step2:Access the Located File
		XSSFWorkbook wb = new XSSFWorkbook(fis);

		// Step3:Count no of Sheets available in excel file
		int countofsheets = wb.getNumberOfSheets();
		System.out.println(countofsheets);

		// Step4: Access the desired sheet
		for (int i = 0; i < countofsheets; i++) {
			String sheetname = wb.getSheetName(i);
			if (sheetname.equals(Sheet_name)) {
				System.out.println("inside the sheet:" + sheetname);
				XSSFSheet sheet = wb.getSheetAt(i);
				Iterator<Row> Rows = sheet.iterator();
				while (Rows.hasNext()) {
					Row currentRow = Rows.next();

					// Step 5: Access the row corresponding desired test case
					if (currentRow.getCell(0).getStringCellValue().equals(Test_Case_Name)) {
						Iterator<Cell> Cell = currentRow.iterator();
						while (Cell.hasNext()) {
							String Data = Cell.next().getStringCellValue();
							System.out.println(Data);
							ArrayData.add(Data);
						}

					}
				}
			}
		}
		wb.close();
		return ArrayData;
	}

}
